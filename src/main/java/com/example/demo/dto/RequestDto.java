package com.example.demo.dto;

import java.util.Date;

public class RequestDto {
	private Integer requestId;
	private Integer customerId;
	private Integer driverId;
	private String status;
	private Date requestPlaced;
	private Date requestInitiated;
	private Date requestProcessed;
	
	public Integer getRequestId() {
		return requestId;
	}
	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getDriverId() {
		return driverId;
	}
	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getRequestPlaced() {
		return requestPlaced;
	}
	public void setRequestPlaced(Date requestPlaced) {
		this.requestPlaced = requestPlaced;
	}
	public Date getRequestInitiated() {
		return requestInitiated;
	}
	public void setRequestInitiated(Date requestInitiated) {
		this.requestInitiated = requestInitiated;
	}
	public Date getRequestProcessed() {
		return requestProcessed;
	}
	public void setRequestProcessed(Date requestProcessed) {
		this.requestProcessed = requestProcessed;
	}
}
