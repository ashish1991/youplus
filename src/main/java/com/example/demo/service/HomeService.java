package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.RequestDto;

public interface HomeService {
	List<RequestDto> getAllRequests();
	void rideNow(Integer customerId);
}
