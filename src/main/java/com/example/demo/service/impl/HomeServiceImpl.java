package com.example.demo.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.converter.RequestConverter;
import com.example.demo.dto.RequestDto;
import com.example.demo.entity.Customer;
import com.example.demo.entity.Request;
import com.example.demo.entity.Status;
import com.example.demo.enums.StatusEnum;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.repository.RequestRepository;
import com.example.demo.service.HomeService;

@Service
public class HomeServiceImpl implements HomeService {

	@Autowired
	RequestRepository requestRepository;
	@Autowired
	CustomerRepository customerRepository;

	@Override
	@Transactional
	public List<RequestDto> getAllRequests() {
		return requestRepository.findAll().stream().map(RequestConverter::entityToDto)
				.collect(Collectors.toList());
	}

	@Override
	@Transactional
	public void rideNow(Integer customerId) {
		Customer customer=customerRepository.findOne(customerId);
		if(customer== null){
			customer= new Customer(customerId);
		}
		Request request = new Request();
		request.setCustomer(customer);
		request.setRequestPlaced(new Date());
		request.setStatus(new Status(StatusEnum.WAITING.getCode()));
		requestRepository.save(request);
	}
}
