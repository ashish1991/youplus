package com.example.demo.converter;

import com.example.demo.dto.RequestDto;
import com.example.demo.entity.Request;

public class RequestConverter {

	public static RequestDto entityToDto(Request request){
		RequestDto requestDto= new RequestDto();
		requestDto.setCustomerId(request.getCustomer().getCustomerId());
		if(request.getDriver() != null)
			requestDto.setDriverId(request.getDriver().getDriverId());
		requestDto.setRequestId(request.getRequestId());
		requestDto.setRequestInitiated(request.getRequestInitiated());
		requestDto.setRequestPlaced(request.getRequestPlaced());
		requestDto.setRequestProcessed(request.getRequestProcessed());
		requestDto.setStatus(request.getStatus().getStatus());
		return requestDto;
	}
}
