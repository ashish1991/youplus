package com.example.demo;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.entity.Customer;
import com.example.demo.entity.Driver;
import com.example.demo.entity.Request;
import com.example.demo.entity.Status;
import com.example.demo.enums.StatusEnum;
import com.example.demo.repository.DriverRepository;
import com.example.demo.repository.RequestRepository;
import com.example.demo.repository.StatusRepository;

@SpringBootApplication
public class BootDemoApplication {

	@Autowired
	DriverRepository driverRepository;
	@Autowired
	StatusRepository statusRepository;
	@Autowired
	RequestRepository requestRepository;

	public static void main(String[] args) {
		SpringApplication.run(BootDemoApplication.class, args);
	}

	@PostConstruct
	public void setupDbWithData() {
		List<Driver> drivers = new LinkedList<>();
		for (int i = 0; i < 5; i++) {
			drivers.add(new Driver());
		}
		driverRepository.save(drivers);
		List<Status> statusList = Arrays.asList(new Status("Waiting"), new Status("Ongoing"), new Status("Completed"));
		statusRepository.save(statusList);
		
		Request request= new Request();
		request.setCustomer(new Customer(4567));
		request.setRequestPlaced(new Date());
		request.setStatus(new Status(StatusEnum.WAITING.getCode()));
		requestRepository.save(request);
	}
}
