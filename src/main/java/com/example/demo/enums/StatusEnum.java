package com.example.demo.enums;

public enum StatusEnum {
	WAITING(1), ONGOING(2), COMPLETE(3);
	private int code;

	private StatusEnum(int code) {
		this.code = code;
	}

	public Integer getCode() {
		return this.code;
	}
}
