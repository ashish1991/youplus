package com.example.demo.entity;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Customer {
	@Column
	@Id
	private Integer customerId;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Request> request= new LinkedList<>();
    
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public List<Request> getRequest() {
		return request;
	}
	public void setRequest(List<Request> request) {
		this.request = request;
	}
	public Customer(Integer customerId) {
		this.customerId = customerId;
	}
	public Customer() {
	}
}
