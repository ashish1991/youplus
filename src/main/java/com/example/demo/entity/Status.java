package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Status {
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer statusId;
	@Column
	private String status;

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Status(String status) {
		this.status = status;
	}

	public Status(Integer statusId) {
		this.statusId = statusId;
	}
	
	public Status() {
	}
}
