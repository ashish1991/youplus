package com.example.demo.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Request {
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private Integer requestId;
	@ManyToOne(cascade = CascadeType.ALL)
	private Customer customer;
	@ManyToOne(cascade = CascadeType.ALL)
	private Driver driver;
	@OneToOne(cascade = CascadeType.DETACH)
	private Status status;
	@Column
	private Date requestPlaced;
	@Column
	private Date requestInitiated;
	@Column
	private Date requestProcessed;

	public Integer getRequestId() {
		return requestId;
	}

	public void setRequestId(Integer requestId) {
		this.requestId = requestId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getRequestPlaced() {
		return requestPlaced;
	}

	public void setRequestPlaced(Date requestPlaced) {
		this.requestPlaced = requestPlaced;
	}

	public Date getRequestInitiated() {
		return requestInitiated;
	}

	public void setRequestInitiated(Date requestInitiated) {
		this.requestInitiated = requestInitiated;
	}

	public Date getRequestProcessed() {
		return requestProcessed;
	}

	public void setRequestProcessed(Date requestProcessed) {
		this.requestProcessed = requestProcessed;
	}
}
