'use strict'

var demoApp = angular.module('demo', [ 'ui.bootstrap', 'demo.controllers',
		'demo.services' ]);
demoApp.constant("CONSTANTS", {
	RIDE_NOW:"/rideNow",
	GET_ALL_REQUEST:"getAllRequests"
});