'use strict'

var module = angular.module('demo.controllers', []);
module.constant("moment", moment);
module.controller("RequestController", ["$scope", "RequestService", "moment",
  function($scope, RequestService, moment) {
	$scope.allRequests= [];
	$scope.getAllRequests= function () {
		RequestService.getAllRequests().then(function(value) {
			$scope.allRequests= value.data;
		}, function(reason) {
			console.log(reason);
		}, function(value) {
			console.log(value);
		})
	};
	$scope.getAllRequests();
	$scope.rideNow = function (customerId) {
    	RequestService.rideNow(customerId).then(function() {
    		$scope.getAllRequests();
    		$scope.activeTab=0;
    	}, function(reason) {
    		
    	}, function(value) {
    		
    	})
    };
    
    $scope.getTimeDiff= function(time){
    	return new moment(time).fromNow();
    };
    $scope.getDiver= function (driverId) {
    	if(!driverId)
    		return "None"
    	return driverId;
    }
    
    $scope.driverSelected= function (driverId) {
    	console.log(driverId);
    }
}]);