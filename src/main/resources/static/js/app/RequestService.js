'use strict'

angular.module('demo.services', []).factory('RequestService',
  ["$http", "CONSTANTS", function($http, CONSTANTS) {
    var service = {};
    service.getRequests = function(userId) {
      var url = "";
      return $http.get(url);
    };
    service.rideNow = function(customerId) {
        return $http.post(CONSTANTS.RIDE_NOW, customerId);
      };
    service.getAllRequests = function () {
    	return $http.get(CONSTANTS.GET_ALL_REQUEST);
    }
    return service;
  }]);